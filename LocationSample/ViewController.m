//
//  ViewController.m
//  LocationSample
//
//  Created by Gaurav Rastogi on 31/03/15.
//  Copyright (c) 2015 Gaurav Rastogi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize locationManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    
    if (locationManager && [locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locationManager requestAlwaysAuthorization];
    }
}

#pragma mark - CLLocationManager Delegate Methods -
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    NSLog(@"locations = %@",locations);
    
    
    CLLocation *newLocation = [locations firstObject];
    
    //set location and zoom level
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(newLocation.coordinate, 2500.0, 2500.0);
    MKCoordinateRegion adjustedRegion = [self.mpView regionThatFits:viewRegion];
    [self.mpView setRegion:adjustedRegion animated:YES];
    
    [self.locationManager stopUpdatingLocation];
    
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)stopLocationManager{
    
    NSLog(@"Location Manager Stopped");
    
    [locationManager stopUpdatingLocation];
}
- (IBAction)onClickOfStopLocationManagerButton {
    
    //[locationManager stopUpdatingLocation];
}

- (IBAction)onClickOfLocateMeButton:(UIButton *)sender {
    
    NSLog(@"Location Manager Started");
    
    [locationManager startUpdatingLocation];
    
    [self performSelector:@selector(stopLocationManager) withObject:nil afterDelay:10.0f];
}

@end
