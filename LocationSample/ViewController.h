//
//  ViewController.h
//  LocationSample
//
//  Created by Gaurav Rastogi on 31/03/15.
//  Copyright (c) 2015 Gaurav Rastogi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController <CLLocationManagerDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mpView;
@property (nonatomic,strong) CLLocationManager *locationManager;


@end

